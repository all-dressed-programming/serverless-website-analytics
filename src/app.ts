import { Router, Context, Request } from "https://deno.land/x/oak/mod.ts";
import { Pool } from "https://deno.land/x/postgres@v0.17.0/mod.ts";
import { createBundle } from "./bundle.ts";
const clientBundle = createBundle(
    new URL("./client.js", import.meta.url),
    new URL("../import_map.json", import.meta.url),
    false,
);

export function buildApp(pool: Pool): Router{
    const router = new Router();
    function getHeaders(request: Request): Record<string, string>{
        const ret: Record<string, string> = {};
        for (const [k, v] of request.headers.entries()) {
            ret[k] = v;
        }
        return ret;
    }
    
    router.post("/api/v1/spy", async (ctx: Context) => {
      const payload = await ctx.request.body().value;
      payload.request = {
        ip: ctx.request.ip,
        ips: ctx.request.ips,
        url: ctx.request.url.toString(),
        date: Date.now(),
        method: ctx.request.method,
        headers: getHeaders(ctx.request),
      };
      const client = await pool.connect();
      try{
          const query ="INSERT INTO blog_statistics(raw_data) VALUES ($1)";
          await client.queryArray(query, [payload]);
          ctx.response.body = '{}';
      }finally {
        client.release()
      }
    });

    router.get("/stats", (ctx:Context) => {
        ctx.response.body = 'hello world' 
    });

    router.get("/toto.js", async (ctx: Context) => {

        ctx.response.body = await clientBundle;
    });

    return router;

}