
import { Application, Router, Context, Request } from "https://deno.land/x/oak/mod.ts";
import { Pool } from "https://deno.land/x/postgres@v0.17.0/mod.ts";
import {buildApp} from "./app.ts";
import { oakCors } from "https://deno.land/x/cors/mod.ts";
import staticFiles from "https://deno.land/x/static_files@1.1.6/mod.ts";


const pool = new Pool(Deno.env.get("POSTGRES_URL"),  2, true);
const app = new Application();

app.use(oakCors({origin: /^.+localhost:8080$/,}));
app.use(buildApp(pool).routes());
app.use(staticFiles("public", {brotli: true, fetch: true}));

await app.listen({port:8000});
