(function(window){
    const SPY_KEY = 'spy-key';

    function randomWord(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for(let i = 0; i < length; i++){
            result += characters.charAt(Math.floor(Math.random() * characters.length));
        }
        return result;
    }
    

    function getSpyKey(){
        return window.localStorage.getItem(SPY_KEY);
    }

    function generateSpyKey(){
        const spyKey = randomWord(80);
        window.localStorage.setItem(SPY_KEY, spyKey);
        return spyKey;
    }

    function getOrCreateSpyKey(){
        return getSpyKey() || generateSpyKey()
    }

    async function canvasFingerprint() {

        let canvasElement = document.createElement('canvas');
        let canvasContext = canvasElement.getContext('2d');
        canvasContext.fillStyle = 'rgba(0,0,0,0.5)';
    
        canvasContext.fillText('Hello mon coco ☀️ 🙋🏽', 0, 100)

        canvasContext.fillText('d\ne\tdkd', 50, 50)
        canvasContext.beginPath()
        canvasContext.arc(20, 20, 20, 0, 6)
        canvasContext.fillStyle = 'red'
        canvasContext.fill()    
        let canvasData = (new TextEncoder).encode(canvasElement.toDataURL())
        let canvasFingerprint = await crypto.subtle.digest('sha-1', canvasData)
    
        canvasFingerprint = new Uint8Array(canvasFingerprint)
        canvasFingerprint = String.fromCharCode(...canvasFingerprint)
        canvasFingerprint = btoa(canvasFingerprint)
    
        return canvasFingerprint 
    }

    function getSizes(){
        return {
            'window_innerWidth': window.innerWidth,
            'window_outerWidth': window.outerWidth,
            'window_innerHeight': window.innerHeight,
            'window_outerHeight': window.outerHeight,
        }
    }

    async function getPayload(){
        return {
            windowPerformance: window.performance,
            sizes: getSizes(),
            localStorageSpyKey: getOrCreateSpyKey(),
            canvasFingerPrint: await canvasFingerprint(),
        };
    }

    async function publishPayload(payload){
        const rawResponse = await fetch('/api/v1/spy', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
          });
          const content = await rawResponse.json();
        

    }



    async function main(){
        await publishPayload(await getPayload());
    }

    window.setTimeout(main, 1000);
})(window)
